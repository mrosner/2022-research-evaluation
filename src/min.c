/*******************************************************************************
* Name        : evaluation/src/min.c                                           *
* Author      : M.E.Rosner                                                     *
* E-Mail      : marty[at]rosner[dot]io                                         *
* Version     : 2022Q4                                                         *
* Copyright   : Copyright (C) 2022 M.E.Rosner; Berlin; Germany                 *
* License     : The MIT License                                                *
* Description : evaluation is a collection of formulas                         *
*******************************************************************************/

#include "min.h"

double *
min(double *p_value, int length) {
  double *result = NULL;
  result = &p_value[0];

  for ( int i = 1; i < length; i++ ) {
    // printf("p_value[%i]: %lf\n", i, p_value[i]);
    if ( &p_value[i] < result ) {
      result = &p_value[i];
    }
  }

  // printf("d_min: %lf\n", result);
  return result;
}

/* $evaluation: src/min.c,v 2022Q4 2022/11/17 13:42:20 mer Exp $ */
