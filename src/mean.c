/*******************************************************************************
* Name        : evaluation/src/mean.c                                          *
* Author      : M.E.Rosner                                                     *
* E-Mail      : marty[at]rosner[dot]io                                         *
* Version     : 2022Q4                                                         *
* Copyright   : Copyright (C) 2022 M.E.Rosner; Berlin; Germany                 *
* License     : The MIT License                                                *
* Description : evaluation is a collection of formulas                         *
*******************************************************************************/

#include "mean.h"
#include "array.h"
#include "sum.h"

double *
mean(double *p_value, int length) {
  double *p_sum = NULL; // = calloc(1, sizeof(double)); // NULL;
  p_sum = sum(p_value, length);
  // printf("mean.c, mean, result: %lf\n", *p_sum);

  // for ( int i = 0; i < length; i++ ) {
  //   printf("mean.c, mean, p_value[%i]: %lf\n", i, p_value[i]);
  // }

  double N = (double) length;
  // printf("mean.c, N: %lf\n", N);

  double sum = *p_sum;
  // printf("mean.c, sum: %lf\n", sum);

  double mean = sum / N;
  // printf("mean.c, mean: %lf\n", mean);

  //double *p_mean = p_sum;
  //*p_mean = mean;
  //printf("mean.c, p_mean: %lf\n", p_mean);

  free(p_sum); p_sum = NULL;

  double *p_mean = calloc(1, sizeof(double)); // NULL;
  *p_mean = mean;
  return p_mean;
}

/* $evaluation: src/mean.c,v 2022Q4 2022/11/17 13:42:20 mer Exp $ */
