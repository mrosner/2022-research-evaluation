/*******************************************************************************
* Name        : evaluation/src/mean.h                                          *
* Author      : M.E.Rosner                                                     *
* E-Mail      : marty[at]rosner[dot]io                                         *
* Version     : 2022Q4                                                         *
* Copyright   : Copyright (C) 2022 M.E.Rosner; Berlin; Germany                 *
* License     : The MIT License                                                *
* Description : evaluation is a collection of formulas                         *
*******************************************************************************/

//#ifndef ARRAY_H
//#define ARRAY_H
//#include "array.h"
//#endif

//#ifndef SUM_H
//#define SUM_H
//#include "sum.h"
//#endif

//#include "sum.h"

double *
mean(double *p_value, int length);

/* $evaluation: src/mean.h,v 2022Q4 2022/11/17 13:42:20 mer Exp $ */
