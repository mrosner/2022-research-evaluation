/*******************************************************************************
* Name        : evaluation/src/max.c                                           *
* Author      : M.E.Rosner                                                     *
* E-Mail      : marty[at]rosner[dot]io                                         *
* Version     : 2022Q4                                                         *
* Copyright   : Copyright (C) 2022 M.E.Rosner; Berlin; Germany                 *
* License     : The MIT License                                                *
* Description : evaluation is a collection of formulas                         *
*******************************************************************************/

#include "max.h"

double *
max(double *p_value, int length) {
  double *result = NULL;
  result = &p_value[0];

  for ( int i = 1; i < length; i++ ) {
    // printf("maximum: for: p_value[%i]: %lf\n", i, p_value[i]);
    if ( &p_value[i] > result ) {
      result = &p_value[i];
    }
  }

  // double *p_result = calloc(1, sizeof(double));
  printf("return: result: %lf\n", *result);
  // p_result = result;
  return (double *)result; // p_result;
}

/* $evaluation: src/max.c,v 2022Q4 2022/11/17 13:42:20 mer Exp $ */
