/*******************************************************************************
* Name        : evaluation/src/sum.c                                           *
* Author      : M.E.Rosner                                                     *
* E-Mail      : marty[at]rosner[dot]io                                         *
* Version     : 2022Q4                                                         *
* Copyright   : Copyright (C) 2022 M.E.Rosner; Berlin; Germany                 *
* License     : The MIT License                                                *
* Description : evaluation is a collection of formulas                         *
*******************************************************************************/

#include "sum.h"

double *
sum(double *p_value, int length) {
  // set default value first
  double *result = NULL;
  result = calloc(1, sizeof(double)); // NULL;

  // interate over the n-1 rest
  for ( int i = 0; i < length; i++ ) {
    *result = (double) (*result + (double) p_value[i]);
  }

  //printf("sum.c, sum: %lf\n", result);
  return result;
}

/* $evaluation: src/sum.c,v 2022Q4 2022/11/17 13:42:20 mer Exp $ */
