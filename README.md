# evaluation is a collection of formulas
## Vision
To remember and keep formulas reusable as open source, even in business.

## Goal
- C as a choice
- writing clean code
- providing documentation

## Task
- selected formulas
- examples included

init: 2022-11-17T14-10-28Z M.E.Rosner
